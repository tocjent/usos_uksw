package com.example.projekt;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

//fragment wyswietlajacy plan

public class PlanFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_plan, container,
				false);
		Context context = MainActivity.getAppContext();

		String plan[][] = LoginActivity.plan;
		int i=0, j=0;

		TableLayout table = (TableLayout) rootView.findViewById(R.id.plan);

		
		
		for(j=0; j<5; j++){
			for(i=0; i<58; i++){
				
				TableRow row = new TableRow(context);
				TextView tabPlan = new TextView(context);
		      	tabPlan.setTextColor(Color.parseColor("#000000"));
				
				if(i==0){
					tabPlan.setBackgroundResource(R.drawable.tab_shape);
				} else {
					tabPlan.setBackgroundColor(Color.parseColor("#d0d0d0"));
				}
		      	
		      	if(plan[i][j]!=null){
					if(!plan[i][j].startsWith(" ")){
		      		tabPlan.setText(plan[i][j]);
					row.addView(tabPlan);
					table.addView(row);
					}
				}
				

				
			}
		}

		return rootView;
	}
}
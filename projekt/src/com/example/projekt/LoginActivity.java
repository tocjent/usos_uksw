package com.example.projekt;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.google.common.base.CharMatcher;

/**
 * Aktywnosc wyswietlajaca formularz logowania
 */
public class LoginActivity extends Activity {
	
	//dane do wyswietlenia
	public static ArrayList<String> oceny;
	public static ArrayList<String> nazwy_przedmiotow;
	public static String[][] plan;
	
	public static final String EXTRA_EMAIL = "com.example.android.authenticatordemo.extra.EMAIL";

	//zmienna umozliwiajaca anulowanie logowania
	private UserLoginTask mAuthTask = null;

	// wartosci zmiennych w momencie logowania
	private static String mUsername;
	private static String mPassword;

	// elementy interfejsu
	private static EditText mUsernameView;
	private static EditText mPasswordView;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login);

		// interfejs logowania
		mUsername = getIntent().getStringExtra(EXTRA_EMAIL);
		mUsernameView = (EditText) findViewById(R.id.email);
		mUsernameView.setText(mUsername);

		mPasswordView = (EditText) findViewById(R.id.password);
		mPasswordView
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_NULL) {
							attemptLogin();
							return true;
						}
						return false;
					}
				});

		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

		//po nacisnieciu przycisku sprobuj sie zalogowac
		findViewById(R.id.sign_in_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						attemptLogin();
					}
				});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	/**
	 * proba logowania, sprawdzenie danych przed polaczeniem z internetem
	 */
	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		mUsernameView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		mUsername = mUsernameView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// sprawdz czy haslo jest wlasciwe
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 3) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		// sprawdz czy uzytkownik jest wlasciwy
		if (TextUtils.isEmpty(mUsername)) {
			mUsernameView.setError(getString(R.string.error_field_required));
			focusView = mUsernameView;
			cancel = true;
		} else if (CharMatcher.anyOf("QWERTYUIOPASDFGHJKLZXCVBNM")
				.matchesAnyOf(mUsername)) {
			mUsernameView.setError(getString(R.string.error_invalid_email));
			focusView = mUsernameView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			mAuthTask = new UserLoginTask();
			mAuthTask.execute();
		}
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.
			try {
				usos_conn(mUsername, mPassword);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace(System.out);
				return false;
			}

			// TODO: register the new account here.
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			showProgress(false);

			if (success) {
				startActivity(new Intent(getApplicationContext(), MainActivity.class));
			} else {
				mPasswordView
						.setError(getString(R.string.error_incorrect_password));
				mPasswordView.requestFocus();
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}

	public void usos_conn(String username, String password) throws Exception {
		Document doc;
		InputStream in;
		Element ltInput, executionInput, plan_html;
		String lt, execution;
		Elements oceny_html, nazwy_przedmiotow_html, rzedy;
		
		oceny = new ArrayList<String>();
		nazwy_przedmiotow = new ArrayList<String>();
		plan = new String[58][6];
		
		CookieStore cookieStore = new BasicCookieStore();	
		
		HttpClient httpclient = new DefaultHttpClient();
		
		HttpContext httpContext = new BasicHttpContext();
		httpContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
		
		try {
			HttpGet httpget = new HttpGet("https://login.uksw.edu.pl/cas/login");
			httpget.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36");
			httpget.setHeader("Referer", "https://login.uksw.edu.pl/cas/login");
			httpget.setHeader("Host", "login.uksw.edu.pl");
			
			
			HttpResponse response1 = httpclient.execute(httpget, httpContext);
			try {
				HttpEntity entity = response1.getEntity();
				in = response1.getEntity().getContent();
				doc = Jsoup.parse(readStream(in));				
				ltInput = doc.select("input[name=lt]").first();
				executionInput = doc.select("input[name=execution]").first();
				lt = ltInput.attr("value");
				execution = executionInput.attr("value");
				entity.consumeContent();
			} finally {
				//response1.close();
			}
			
			HttpPost httpost = new HttpPost("https://login.uksw.edu.pl/cas/login");
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("username", username));
			nvps.add(new BasicNameValuePair("password", password));
			nvps.add(new BasicNameValuePair("lt", lt));
			nvps.add(new BasicNameValuePair("execution", execution));
			nvps.add(new BasicNameValuePair("_eventId", "submit"));
			nvps.add(new BasicNameValuePair("submit", ""));
			httpost.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36");
			httpost.setHeader("Referer", "https://login.uksw.edu.pl/cas/login");
			httpost.setHeader("Host", "login.uksw.edu.pl");
			
			httpost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));

			response1 = httpclient.execute(httpost, httpContext);
			try {
				HttpEntity entity = response1.getEntity();
				in = response1.getEntity().getContent();
				doc = Jsoup.parse(readStream(in));
				entity.consumeContent();
			} finally {
				//response1.close();
			}
			
			
			httpget = new HttpGet("https://usosweb.uksw.edu.pl/kontroler.php?_action=actionx:dla_stud/studia/oceny/index()");
			httpget.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36");
			httpget.setHeader("Referer", "https://login.uksw.edu.pl/cas/login");
			httpget.setHeader("Host", "usosweb.uksw.edu.pl");
			
			HttpResponse response2 = httpclient.execute(httpget, httpContext);	
			
			try {
				HttpEntity entity = response2.getEntity();
				in = response2.getEntity().getContent();
				doc = Jsoup.parse(readStream(in));
				nazwy_przedmiotow_html = doc.select("a[href*=/przedmioty/pokazPrzed]");
				for (Element e : nazwy_przedmiotow_html) {
					nazwy_przedmiotow.add(e.text());
				}
				oceny_html = doc.select("td[style*=text-align:right; white-space:nowrap]");
				for (Element e : oceny_html) {
					oceny.add(e.text());
				}
				entity.consumeContent();
			} finally {
				//response2.close();
			}
			
			httpget = new HttpGet("https://usosweb.uksw.edu.pl/kontroler.php?_action=actionx:home/plan(plan_format:html)");
			httpget.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36");
			httpget.setHeader("Referer", "https://login.uksw.edu.pl/cas/login");
			httpget.setHeader("Host", "usosweb.uksw.edu.pl");
			
			HttpResponse response3 = httpclient.execute(httpget, httpContext);	
			
			try {
				HttpEntity entity = response3.getEntity();
				in = response3.getEntity().getContent();
				doc = Jsoup.parse(readStream(in));
				plan_html = doc.select("div[style=margin: 20px 0 6px 0]").first();
				rzedy = plan_html.select("tr");

				
				int i = 0, j;
				for (Element tr : rzedy) {
					j = 0;

					for (Element td : tr.select("td")) {

						if (td.text().length() == 5 || td.text().length() == 4) {

						} else if (td.text().toString().startsWith("Legenda")) {

						} else if (td.toString().contains("<style='background-color:FFFFFF'")) {

						} else if (td.hasText()) {
							plan[i][j] = td.text();
							j++;
						} else if (td.previousElementSibling() != null && td.previousElementSibling().hasText() && !(td.previousElementSibling().text().length() == 5 || td.previousElementSibling().text().length() == 4)) {
						
						} else {
							plan[i][j] = " ";
							j++;
						}
					}
					if (j != 0) {
						i++;
					}
				}
				plan[0][0] = "Poniedzialek";
				plan[0][1] = "Wtorek";
				plan[0][2] = "Sroda";
				plan[0][3] = "Czwartek";
				plan[0][4] = "Piatek";

				entity.consumeContent();
			} finally {
				//response3.close();
			}
		} finally {
			//httpclient.close();
		}
		
	}

	public static String getUsername(){
		return mUsername;
	}
	
	public static String getPassword(){
		return mPassword;
	}
	
	public static void clearData(){
		
		int i, j;
		for(i=0; i<58; i++){
			for(j=0; j<6; j++){
				plan[i][j]=null;
			}
		}
		
		nazwy_przedmiotow.removeAll(nazwy_przedmiotow);
		oceny.removeAll(oceny);
		mUsernameView.setText("");
		mPasswordView.setText("");
		
	}
	
	private static String readStream(InputStream in) throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		StringBuilder out = new StringBuilder();
		String line;
		while ((line = reader.readLine()) != null) {
			out.append(line);
		}
		reader.close();
		return out.toString();
	}
}

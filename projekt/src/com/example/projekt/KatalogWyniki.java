package com.example.projekt;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class KatalogWyniki extends Activity {

	static List<String> przedmioty = new ArrayList<String>();
	static List<String> programy = new ArrayList<String>();
	static List<String> jednostki = new ArrayList<String>();
	static List<String> osoby = new ArrayList<String>();
	private static Context context;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_katalog_wyniki);

		przedmioty = KatalogFragment.przedmioty;
		programy = KatalogFragment.programy;
		jednostki = KatalogFragment.jednostki;
		osoby = KatalogFragment.osoby;
		
		
		Context context = MainActivity.getAppContext();

		TableLayout table = (TableLayout) findViewById(R.id.search_table);
		table.removeAllViews();
		TableRow row;
		TextView tab;
		
		
		row = new TableRow(context);
		tab = new TextView(context);
		
		tab.setTextColor(Color.parseColor("#000000"));
		tab.setBackgroundResource(R.drawable.tab_shape);
		
		tab.setText("Osoby");
		row.addView(tab);
		table.addView(row);
		
		for(String e : osoby){
			row = new TableRow(context);
			tab = new TextView(context);
			tab.setTextColor(Color.parseColor("#000000"));
				
			tab.setBackgroundColor(Color.parseColor("#d0d0d0"));
			
			tab.setText(e);
			row.addView(tab);
			table.addView(row);
		}
		
		row = new TableRow(context);
		tab = new TextView(context);
		
		tab.setTextColor(Color.parseColor("#000000"));
		tab.setBackgroundResource(R.drawable.tab_shape);
		
		tab.setText("Programy");
		row.addView(tab);
		table.addView(row);
		
		for(String e : programy){
			row = new TableRow(context);
			tab = new TextView(context);
			tab.setTextColor(Color.parseColor("#000000"));
				
			tab.setBackgroundColor(Color.parseColor("#d0d0d0"));
			
			tab.setText(e);
			row.addView(tab);
			table.addView(row);
		}
		
		row = new TableRow(context);
		tab = new TextView(context);
		tab.setTextColor(Color.parseColor("#000000"));
	
		tab.setBackgroundResource(R.drawable.tab_shape);
		
		tab.setText("Przedmioty");
		row.addView(tab);
		table.addView(row);
		
		for(String e : przedmioty){
			row = new TableRow(context);
			tab = new TextView(context);
			tab.setTextColor(Color.parseColor("#000000"));
				
			tab.setBackgroundColor(Color.parseColor("#d0d0d0"));
			
			tab.setText(e);
			row.addView(tab);
			table.addView(row);
		}
		
		row = new TableRow(context);
		tab = new TextView(context);
		tab.setTextColor(Color.parseColor("#000000"));
	
		tab.setBackgroundResource(R.drawable.tab_shape);
		
		tab.setText("Jednostki");
		row.addView(tab);
		table.addView(row);
		
		for(String e : jednostki){
			row = new TableRow(context);
			tab = new TextView(context);
			tab.setTextColor(Color.parseColor("#000000"));
				
			tab.setBackgroundColor(Color.parseColor("#d0d0d0"));
			
			tab.setText(e);
			row.addView(tab);
			table.addView(row);
		}
	}

	
	public static Context getAppContext() {
        return KatalogWyniki.context;
    }
}

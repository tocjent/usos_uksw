package com.example.projekt;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
 
//fragment wyswietlajacy formularz wyszukiwania

public class KatalogFragment extends Fragment {

	//pobierz dane do logowania potrzebne przy wyszukiwaniu
	protected String password=LoginActivity.getPassword(); 
	protected String username=LoginActivity.getUsername();
	//wzorzec do wyszukania
	protected String pattern;
	private Button search_button;
	private EditText query;
	private View searchFormView;
	private View searchStatusView;

	//listy wynikow
	static List<String> przedmioty;
	static List<String> programy;
	static List<String> jednostki;
	static List<String> osoby;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_katalog, container,
				false);
		
		query=(EditText)rootView.findViewById(R.id.search_textfield);
		
		searchFormView = rootView.findViewById(R.id.search_form);
		searchStatusView = rootView.findViewById(R.id.search_status);
		
		search_button=(Button) rootView.findViewById(R.id.search_button);
		search_button.setOnClickListener(new View.OnClickListener() 
        {               
            @Override
            public void onClick(View arg0) 
            {
            	przedmioty = new ArrayList<String>();
            	programy = new ArrayList<String>();
            	jednostki = new ArrayList<String>();
            	osoby = new ArrayList<String>();
            	
                pattern=query.getText().toString();
                SearchTask wyszukiwanie;
                wyszukiwanie = new SearchTask();
                showProgress(true);
    			wyszukiwanie.execute();    			
            }
        });
		
		return rootView;
	}
	
	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			searchStatusView.setVisibility(View.VISIBLE);
			searchStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							searchStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			searchFormView.setVisibility(View.VISIBLE);
			searchFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							searchFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			searchStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			searchFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}
	
	public class SearchTask extends AsyncTask<Void, Void, Boolean> {
		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				search(username, password, pattern);
			} catch (Exception e1) {
				return false;
			}
			

			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			showProgress(false);
			
			if (success) {
				startActivity(new Intent(MainActivity.getAppContext(), KatalogWyniki.class));
			} 
		}

		@Override
		protected void onCancelled() {
			
		}
	}
	
	//funkcja wyszukiwania(loguje, wyszukuje i umieszcza dane w zmiennych)
	public void search(String username, String password, String pattern) throws Exception {
		Document doc;
		InputStream in;
		Element ltInput, executionInput;
		String lt, execution;
		Elements przedmioty_html, programy_html, jednostki_html, osoby_html;
		

		CookieStore cookieStore = new BasicCookieStore();	
		
		HttpClient httpclient = new DefaultHttpClient();
		
		HttpContext httpContext = new BasicHttpContext();
		httpContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
		
		try {
			HttpGet httpget = new HttpGet("https://login.uksw.edu.pl/cas/login");
			httpget.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36");
			httpget.setHeader("Referer", "https://login.uksw.edu.pl/cas/login");
			httpget.setHeader("Host", "login.uksw.edu.pl");
			
			
			HttpResponse response1 = httpclient.execute(httpget, httpContext);
			try {
				HttpEntity entity = response1.getEntity();

				in = response1.getEntity().getContent();

				doc = Jsoup.parse(readStream(in));
				
				ltInput = doc.select("input[name=lt]").first();
				executionInput = doc.select("input[name=execution]").first();
				
				
				lt = ltInput.attr("value");
				execution = executionInput.attr("value");
				

				entity.consumeContent();
				
			} finally {
				//response1.close();
			}
			
			HttpPost httpost = new HttpPost("https://login.uksw.edu.pl/cas/login");
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("username", "86425"));
			nvps.add(new BasicNameValuePair("password", "KQL$ERF31"));
			nvps.add(new BasicNameValuePair("lt", lt));
			nvps.add(new BasicNameValuePair("execution", execution));
			nvps.add(new BasicNameValuePair("_eventId", "submit"));
			nvps.add(new BasicNameValuePair("submit", ""));
			httpost.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36");
			httpost.setHeader("Referer", "https://login.uksw.edu.pl/cas/login");
			httpost.setHeader("Host", "login.uksw.edu.pl");

			
			httpost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));

			response1 = httpclient.execute(httpost, httpContext);
			try {
				HttpEntity entity = response1.getEntity();
				in = response1.getEntity().getContent();
				doc = Jsoup.parse(readStream(in));
				entity.consumeContent();
			} finally {
				//response1.close();
			}
			
			
			httpget = new HttpGet("https://usosweb.uksw.edu.pl/kontroler.php?_action=actionx%3Akatalog2%2Fprzedmioty%2FszukajPrzedmiotu%28%29&_prz_kod=&_pattern="+pattern);
			httpget.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36");
			httpget.setHeader("Referer", "https://login.uksw.edu.pl/cas/login");
			httpget.setHeader("Host", "usosweb.uksw.edu.pl");
			
			HttpResponse response2 = httpclient.execute(httpget, httpContext);	
			
			try {
				HttpEntity entity = response2.getEntity();

				in = response2.getEntity().getContent();

				doc = Jsoup.parse(readStream(in));
				
				przedmioty_html = doc.select("a[href*=/przedmioty/pokazPrzed]").not("a[class=wrblue]");
				for (Element e : przedmioty_html) {
					przedmioty.add(e.text());
				}
				entity.consumeContent();
			} finally {
				//response2.close();
			}
			
			httpget = new HttpGet("https://usosweb.uksw.edu.pl/kontroler.php?_action=actionx%3Akatalog2%2Fprogramy%2FszukajProgramu%28%29&_prg_kod=&_pattern="+pattern);
			httpget.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36");
			httpget.setHeader("Referer", "https://login.uksw.edu.pl/cas/login");
			httpget.setHeader("Host", "usosweb.uksw.edu.pl");
			
			HttpResponse response3 = httpclient.execute(httpget, httpContext);	
			
			try {
				HttpEntity entity = response3.getEntity();

				in = response3.getEntity().getContent();

				doc = Jsoup.parse(readStream(in));
				
				programy_html = doc.select("td.moregrey").not("a[class=wrblue]");
				for (Element e : programy_html) {
					if(!e.text().startsWith("Opis")){
						programy.add(e.text());
					}
				}
				entity.consumeContent();
				} finally {
				//response3.close();
			}
			
			httpget = new HttpGet("https://usosweb.uksw.edu.pl/kontroler.php?_action=actionx%3Akatalog2%2Fjednostki%2FszukajJednostki%28%29&_jed_org_kod=&_pattern="+pattern);
			httpget.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36");
			httpget.setHeader("Referer", "https://login.uksw.edu.pl/cas/login");
			httpget.setHeader("Host", "usosweb.uksw.edu.pl");
			
			HttpResponse response4 = httpclient.execute(httpget, httpContext);	
			
			try {
				HttpEntity entity = response4.getEntity();
				in = response4.getEntity().getContent();
				doc = Jsoup.parse(readStream(in));
				jednostki_html = doc.select("tr[class*=row]").select("div");
				for (Element e : jednostki_html) {
					jednostki.add(e.text());
				}
				entity.consumeContent();
			} finally {
				//response4.close();
			}
			
			httpget = new HttpGet("https://usosweb.uksw.edu.pl/kontroler.php?_action=actionx%3Akatalog2%2Fosoby%2FszukajOsoby%28%29&_os_id=&_pattern="+pattern+"&_among=both");
			httpget.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36");
			httpget.setHeader("Referer", "https://login.uksw.edu.pl/cas/login");
			httpget.setHeader("Host", "usosweb.uksw.edu.pl");
			
			HttpResponse response5 = httpclient.execute(httpget, httpContext);	
			
			try {
				HttpEntity entity = response5.getEntity();
				in = response5.getEntity().getContent();
				doc = Jsoup.parse(readStream(in));
				osoby_html = doc.select("td.strong");
				for (Element e : osoby_html) {
					if(!e.text().startsWith("Strona")){
						osoby.add(e.text());
					}
				}
				entity.consumeContent();
			} finally {
				//response5.close();
			}
		} finally {
			//httpclient.close();
		}
		
	}
	private static String readStream(InputStream in) throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		StringBuilder out = new StringBuilder();
		String line;
		while ((line = reader.readLine()) != null) {
			out.append(line);
		}
		reader.close();
		return out.toString();
	}
	
	
}
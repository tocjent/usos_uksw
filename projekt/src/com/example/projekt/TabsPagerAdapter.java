package com.example.projekt;

import com.example.projekt.PlanFragment;
import com.example.projekt.OcenyFragment;
import com.example.projekt.KatalogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
 
public class TabsPagerAdapter extends FragmentPagerAdapter {
 
    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }
 
    @Override
    public Fragment getItem(int index) {
 
        switch (index) {
        case 0:
            // Top Rated fragment activity
            return new OcenyFragment();
        case 1:
            // Games fragment activity
            return new PlanFragment();
        case 2:
            // Movies fragment activity
            return new KatalogFragment();
        }
 
        return null;
    }
 
    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 3;
    }
 
}
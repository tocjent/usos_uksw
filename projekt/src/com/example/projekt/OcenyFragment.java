package com.example.projekt;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
 
//fragment wyswietlajacy oceny

public class OcenyFragment extends Fragment {
 
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
    	Context context=MainActivity.getAppContext();
    	
        View rootView = inflater.inflate(R.layout.fragment_oceny, container, false);
        
        List <String> oceny = LoginActivity.oceny;
        List <String> nazwy_przedmiotow = LoginActivity.nazwy_przedmiotow;
        
        TableLayout table = (TableLayout) rootView.findViewById(R.id.tabela_ocen);
        
        TableRow row = new TableRow(context);
        TextView tabPrzedmioty = new TextView(context);
        TextView tabOceny = new TextView(context);
        
        tabPrzedmioty.setText("Nazwa przedmiotu:");
        tabOceny.setText("Ocena:");
        
        tabPrzedmioty.setBackgroundResource(R.drawable.tab_shape);
        tabOceny.setBackgroundResource(R.drawable.tab_shape);
        
        tabPrzedmioty.setTextColor(Color.parseColor("#000000"));
        tabOceny.setTextColor(Color.parseColor("#000000"));
        
        row.addView(tabPrzedmioty);
        row.addView(tabOceny);
 
        table.addView(row);
        
        for(int i=0;i<nazwy_przedmiotow.size();i++)
        {
            row=new TableRow(context);
            
            String ocena = oceny.get(i);
            String przedmiot = nazwy_przedmiotow.get(i);
            
            tabPrzedmioty=new TextView(context);
            tabPrzedmioty.setText(""+przedmiot);
            
            tabOceny=new TextView(context);
            tabOceny.setText(""+ocena);
            
            tabPrzedmioty.setTextColor(Color.parseColor("#000000"));
            tabOceny.setTextColor(Color.parseColor("#000000"));
            
            tabPrzedmioty.setBackgroundColor(Color.parseColor("#d0d0d0"));
            tabOceny.setBackgroundColor(Color.parseColor("#d0d0d0"));

            
            row.addView(tabPrzedmioty);
            row.addView(tabOceny);
            
            table.addView(row);
        }


        
        return rootView;
    }
}